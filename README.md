Education Platform
---


<!-- Deploy on localhost -->
mkcert -install
mkcert localhost

<!-- Deploy on staging,production -->
Install pm2

pm2 start yarn --interpreter bash --name ui -- prod
